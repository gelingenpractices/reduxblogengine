import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { showPost, deletePost } from '../actions/index';
import { Link } from 'react-router';

class PostsOpen extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentWillMount(){
        this.props.showPost(this.props.params.id);
    }

    onDeleteClick(){
        this.props.deletePost(this.props.params.id)
            .then(() => {
                this.context.router.push("/");
            });
    }

    render(){
        if(!this.props.post){
            return (<div>Loading...</div>);
        }
        return (
            <div>
                <h3>{ this.props.post.title }</h3>
                <h6>{ this.props.post.categories }</h6>
                <p>{ this.props.post.content }</p>

                <Link to="/" className="btn btn-secondary">Back</Link>
                <button className="btn btn-danger pull-xs-right"
                onClick={this.onDeleteClick.bind(this)}
                >Delete</button>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        post: state.posts.post
    };
}

export default connect(mapStateToProps, { showPost, deletePost })(PostsOpen);