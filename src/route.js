import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import PostsIndex from './components/posts_index';
import PostsNew from './components/post_new';
import PostsOpen from './components/post_open';

export default (
    <Route path="/" component={ App }>
        <IndexRoute component={PostsIndex} />
        <Route path="posts/new" component={PostsNew} />
        <Route path="posts/:id" component={PostsOpen} />
    </Route>
);